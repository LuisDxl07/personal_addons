{
    'name': 'Curso Systeg',
    'version': '1.1',
    'category': 'Accounting',
    'depends' : [
        'base',
        'mail',
        'account',
        'purchase',
    ],
    'description': """
This module helps to configure the system at the installation of a new database.
================================================================================

Este es un modulo de pruebas.

    """,
    'data': [
        'data/sequence.xml',
        'security/security.xml', #Agrega los grupos para poder usarlo en el siguiente archivo
        'security/ir.model.access.csv', #necesita los grupos creados en el archivo anterior
        'views/account_move_curso_view.xml',
        'views/res_partner_view.xml',
        'views/res_users_view.xml',
        'views/purchase_order_view.xml',
        'views/account_invoice_view.xml',
        'views/account_move_curso_tag_view.xml',
        'report/report_account_move_curso.xml',
        'report/purchase_order_report_new.xml',
    ],
    'installable': True,
    'auto_install': False,
}