# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from logging import error
from odoo import api, fields, models
from odoo.exceptions import UserError #siempre culpar al usuario :3


class AccountMoveCursoTag(models.Model):
    _name = 'account.move.curso.tag'
    _description = "Categorias del curso"
    
    name = fields.Char(string="Nombre Categoria")