# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    curso_id = fields.Many2one('account.move.curso', string="Curso", readonly=True)