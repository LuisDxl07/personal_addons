# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from logging import error
from odoo import api, fields, models
from odoo.exceptions import UserError #siempre culpar al usuario :3


class ModelStockPickingInherit(models.Model):
    _inherit = 'stock.picking'
    _description = "Herencia al modulo de Stock Picking para Inventarios"

    detalles = fields.Char(string="Detalles")
    fecha_final = fields.Date(string="Fecha de termino")
    num_contacto = fields.Char(string="Telefono de contacto")
    direccion = fields.Char(string="Direccion")
    oficina = fields.Integer(string="Num. Oficina")
    encargado = fields.Many2one('res.users', string="Encargado de recibos")
    peticiones = fields.Integer(string="Num. Peticiones")
    costo = fields.Float(string="Costo C/U")
    total = fields.Float(string="Total para entrega", compute="_compute_total_entrega", store=True)
    impuesto = fields.Float(string="Total de impuestos")


    folio = fields.Char(String="Folio", compute="_compute_oficina", store=True)

    @api.depends('oficina')
    def _compute_oficina(self):
        for record in self:
            record.folio = "EXIST-" + str(record.oficina) + "-" + record.origin

    
    @api.depends('peticiones', 'costo')
    def _compute_total_entrega(self):
        for record in self:
            record.total = record.peticiones * record.costo
    