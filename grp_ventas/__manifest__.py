{
    'name': 'GRP Ventas Systeg',
    'version': '1.1',
    'category': 'Sales',
    'depends' : [
        'base',
        'sale',
        'account',
        'purchase',
        'stock',
    ],
    'description': """
This module helps to configure the system at the installation of a new database.
================================================================================

Este es un modulo de pruebas.

    """,
    'data': [
        'views/view_stock_picking_inh.xml',
    ],
    'installable': True,
    'auto_install': False,
}