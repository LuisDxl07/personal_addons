{
    'name' : 'Aprendiendo odoo',
    'version': '1.1',
    'depends' : ['base', 'contacts'],
    'data': [
        'views/res_partner_views.xml',
    ],
    'installable': True,
    'auto_install': True,
}
