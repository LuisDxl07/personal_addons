# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class ResPartnerModel(models.Model):
    _inherit = 'res.partner'

    apodo =  fields.Char(string="Apodo: ")